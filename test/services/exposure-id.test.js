const app = require('../../src/app');

describe('\'exposureID\' service', () => {
  it('registered the service', () => {
    const service = app.service('exposure-id');
    expect(service).toBeTruthy();
  });
});
