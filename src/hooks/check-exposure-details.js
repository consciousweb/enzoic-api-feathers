const { GeneralError } = require('@feathersjs/errors');
const EnzoicAPI = require('@enzoic/enzoic');

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    let promise = new Promise((resolve, reject) => {
      const Enzoic = new EnzoicAPI(context.data.key, context.data.secret);

      if (!context.data.exposureID) {
        reject(new GeneralError('\'exposureID\' field is required.'));
      }

      Enzoic.getExposureDetails(
        context.data.exposureID,
        (error, exposureDetails) => {
          if (error) {
            reject(new GeneralError('Error calling API: ' + error));
          }
          context.data = exposureDetails;
          resolve();
        }
      );
    });
    let result = await promise;
    if (result) {
      return context;
    }
  };
};
