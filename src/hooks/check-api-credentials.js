const { GeneralError } = require('@feathersjs/errors');
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    // confirm api key and secret exist

    let promise = new Promise((resolve, reject) => {
      if (!context.data.key) {
        reject(new GeneralError('\'key\' field is required'));
      } else if (!context.data.secret) {
        reject(new GeneralError('\'secret\' field is required'));
      }
      resolve();
    });

    let result = await promise;
    if (result) {
      return context;
    }
  };
};
