// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async (context) => {
    console.log(context.data)
    // pass query to context if query is used instead of json body
    if (Object.keys(context.data).length === 0 && context.params.query) {
      context.data = context.params.query;
    }
    console.log('context data validated');
    return context;
  };
};
