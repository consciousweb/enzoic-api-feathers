const querydata = require('./hooks/querydata');
const checkAPICredentials = require('./hooks/check-api-credentials');
// Application hooks that run for every service

module.exports = {
  before: {
    all: [querydata(), checkAPICredentials()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
