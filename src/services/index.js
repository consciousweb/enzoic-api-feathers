const username = require('./username/username.service.js');
const exposureId = require('./exposure-id/exposure-id.service.js');
const credentials = require('./credentials/credentials.service.js');
const password = require('./password/password.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(username);
  app.configure(exposureId);
  app.configure(credentials);
  app.configure(password);
};
