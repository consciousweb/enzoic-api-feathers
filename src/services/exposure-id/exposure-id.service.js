// Initializes the `exposureID` service on path `/exposure-id`
const { ExposureId } = require('./exposure-id.class');
const hooks = require('./exposure-id.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/exposure-id', new ExposureId(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('exposure-id');

  service.hooks(hooks);
};
